Simple blog using web.py and Mozilla Persona 
======================================

Note
----

This blog software was written in the winter of 2011-2012 when Persona was still called BrowserID. There are probably old API calls pointing to BrowserID, I'll try to fix them  when I find the time.

Description
-----------

The plan is to build a basic, themeable blog software using
web.py as backend and Mozilla Persona (formerly BrowserID) as authentication method.

Install/requirements
--------------------

First of all you obviously need web.py.

    $ git clone https://github.com/webpy/webpy.git
    $ cd webpy
    # python setup.py install

Then you need to install sqlite3 which should be found in
your package manager. Create the database with the command:

    $ sqlite3 sqlitedb

Then, in sqlite:

    sqlite> create table posts (id integer primary key, date text, title text, url text, content text, published integer, author text);
    sqlite> create table comments (id integer primary key, date text, email text, name text, comment text, pid integer);

That's it if I'm not mistaken. Run it with:

    $ python server.py

and direct your browser to http://localhost:8080
